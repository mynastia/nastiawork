import java.util.Scanner;
public class TaskCh10N50 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int m, n, A;
        System.out.println("Введите m");
        m = reader.nextInt();
        System.out.println("Введите n");
        n = reader.nextInt();
        A=getA(m, n);
        System.out.println("Функция Аккермана составлет "+A);
    }

    private static int getA(int m, int n) {
        if (m==0) {
            return n+1;
        } else if (n==0) {
            getA(m-1,1);
            }
        return getA(m-1,
                getA(m-1, n - 1));
    } }

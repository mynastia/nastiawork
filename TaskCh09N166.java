import java.util.Scanner;
public class TaskCh9N166 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        String sentence;
        System.out.println("Введите предложение");
        sentence = reader.nextLine();
        String words[] = sentence.split(" ");
        int lastIndex = words.length;
        String firstWord = words[0];
        String lastWord = words[lastIndex-1];
        words[0] = lastWord;
        words[lastIndex-1] = firstWord;
        for (int n=0; n<=lastIndex-1; n++) {
            System.out.print(words[n]+" ");
        }
    }
}

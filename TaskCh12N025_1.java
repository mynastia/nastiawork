import java.util.Scanner;
public class TaskCh12N25 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int i, j;
        int leng;
        System.out.println("Введите размер массива");
        leng=scanner.nextInt();
        int[][] array = new int[leng][leng];
        for (i=1; i<leng; i++) {
            for (j=1; j<leng; j++) {
                array[i][j]=makePattern(i,j,array);
                System.out.print(array[i][j]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int i2, j2;
        int leng2=13;
        int[][] array2 = new int[leng2][leng2];
        int num2=0;
        int t=0;
        for (i2=1; i2<leng2; i2++) {
            for (j2=1; j2<leng2; j2++) {
                if ((i2==1)&(j2==1)) num2=i2;
                array2[j2][i2]=num2;
                System.out.print(array2[j2][i2]+", ");
                num2=num2+12;
                t=t+1;
                if (t>=12) {
                    num2=i2+1;
                    t = 0;
                }
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int i3, j3;
        int leng3;
        System.out.println("Введите размер следующего массива");
        leng3=scanner.nextInt();
        int[][] array3 = new int[leng3][leng3];
        for (i3=1; i3<leng3; i3++) {
            for (j3=1; j3<leng; j3++) {
                array3[i3][j3]=makePattern3(i3, j3, leng3, array3);
                System.out.print(array3[i3][j3]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int i4, j4;
        int leng4=13;
        int[][] array4 = new int[leng4][leng4];
        int num4=leng4;
        int t4=0;
        for (i4=1; i4<leng4; i4++) {
            for (j4=1; j4<leng4; j4++) {
                if (j4==1) num4=leng4-i4;
                array4[j4][i4]=num4;
                System.out.print(array4[j4][i4]+", ");
               num4=num4+12;
                t4=t4+1;
                if (t4>=12) {
                    num4=leng4-i4;
                    t4 = 0;
                }
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int i5, j5;
        int leng5=13;
        int [][] array5 = new int[leng5][leng5];
        for (i5=1; i5<leng5; i5++) {
            for (j5=1; j5<leng5; j5++) {
                array5[i5][j5]=makePattern5(i5, j5, leng5, array5);
                System.out.print(array5[i5][j5]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int i6, j6;
        int leng6=13;
        int [][] array6 = new int[leng6][leng6];
        for (i6=1; i6<leng6; i6++) {
            for (j6=1; j6<leng6; j6++) {
                array5[i6][j6]=makePattern6(i6, j6, leng6, array6);
                System.out.print(array5[i6][j6]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");


    }

    private static int makePattern6(int i6, int j6, int leng6, int[][]array6) {
        if ((j6==1)&(i6==1))
            array6[i6][j6]=1;
        if (((j6%2)==1)&(i6!=1))
            array6[i6][j6]=makePattern6(i6-1,j6,leng6,array6)+1;
        if (((j6%2)==1)&(i6==1))
            array6[i6][j6]=makePattern6(i6,j6-1,leng6,array6)+1;
        if (((j6%2)==0)&(i6==1))
            array6[i6][j6]=leng6*j6;
        if (((j6%2)==0)&(i6!=1))
            array6[i6][j6]=leng6*j6-i6;
        return array6[i6][j6];
    }

    private static int makePattern5(int i5, int j5, int leng5, int[][] array5) {
        if ((i5==1)&(j5==1))
            array5[i5][j5]=1;
        if (((i5%2)==1)&(j5!=1))
            array5[i5][j5]=makePattern5(i5,j5-1,leng5,array5)+1;
        if (((i5%2)==1)&(j5==1))
            array5[i5][j5]=makePattern5(i5-1,j5,leng5,array5)+1;
        if (((i5%2)==0)&(j5==1))
            array5[i5][j5]=leng5*i5;
        if (((i5%2)==0)&(j5!=1))
            array5[i5][j5]=leng5*i5-j5;
        return array5[i5][j5];
    }

    private static int makePattern3(int i3, int j3, int leng3, int[][] array3) {
        if (i3==1) {
            array3[i3][j3] = leng3-j3;
            return array3[i3][j3]; }
        array3[i3][j3]=makePattern3(i3-1, j3, leng3, array3) + 10;
        return array3[i3][j3];
    }

    private static int makePattern(int i, int j, int[][] array) {
        if (i==1) {
            array[i][j] = j;
            return array[i][j]; }
         array[i][j]=makePattern(i-1, j, array) + 10^(j-1);
        return array[i][j];
        }
    }

import java.util.Scanner;
public class TaskCh4N33 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int n;
        System.out.println("Введите натуральное число");
        n = Integer.valueOf(reader.nextLine());
        /* Находим последнюю цифру числа как остаток от деления на 10. Далее находим остаток
        от деления полученного числа на 2 и через него узнаем, четное число или нечетное.
         */
        if ((n%10)%2==0) System.out.println("Число заканчивается четной цифрой");
        if ((n%10)%2==1) System.out.println("Число заканчивается нечетной цифрой");
    }
}

public class TaskCh12N25_4 {
    public static void main(String args[]) {
        int h, w;
        int lengH, lengW;
        lengH=13;
        lengW=11;
        int[][] array = new int[lengH][lengW];
        for (h=1; h<lengH; h++) {
            for (w=1; w<lengW; w++) {
                array[h][w]=makePattern(h, w, lengH, array);
                System.out.print(array[h][w]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int h2, w2;
        int lengH2, lengW2;
        lengH2=13;
        lengW2=11;
        int[][] array2 = new int[lengH2][lengW2];
        for (h2=1; h2<lengH2; h2++) {
            for (w2=1; w2<lengW2; w2++) {
                array2[h2][w2]=makePattern2(h2, w2, lengW2, array2);
                System.out.print(array2[h2][w2]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int h3, w3;
        int lengH3, lengW3;
        lengH3=13;
        lengW3=11;
        int[][] array3 = new int[lengH3][lengW3];
        for (h3=1; h3<lengH3; h3++) {
            for (w3=1; w3<lengW3; w3++) {
                array3[h3][w3]=makePattern3(h3, w3, lengH3, array3);
                System.out.print(array3[h3][w3]+", ");
            }
            System.out.println(" ");
        }

    }

    private static int makePattern3(int h3, int w3, int lengH3, int[][] array3) {
        if (((w3%2)==1)&(h3==(lengH3-1))) {
            if (w3==1) {
                array3[h3][w3]=120;
                return array3[h3][w3];
            }
            array3[h3][w3]=makePattern3(h3, w3-1, lengH3, array3)-1;
        }
        if (((w3%2)==1)&(h3<(lengH3-1))) {
            array3[h3][w3]=makePattern3(h3+1, w3, lengH3, array3)-1;
        }
        if (((w3%2)==0)&(h3==1)) {
            array3[h3][w3]=makePattern3(h3, w3-1, lengH3, array3)-1;
        }
        if (((w3%2)==0)&(h3>1)) {
            array3[h3][w3]=makePattern3(h3-1, w3, lengH3, array3)-1;
        }
        return array3[h3][w3];
    }


    private static int makePattern2(int h2, int w2, int lengW2, int[][] array2) {
        if (((h2%2)==1)&(w2==(lengW2-1))) {
            if (h2==1) {
                array2[h2][w2]=120;
                return array2[h2][w2];
            }
            array2[h2][w2]=makePattern2(h2-1, w2, lengW2, array2)-1;
        }
        if (((h2%2)==1)&(w2<(lengW2-1))) {
            array2[h2][w2]=makePattern2(h2, w2+1, lengW2, array2)-1;
        }
        if (((h2%2)==0)&(w2==1)) {
            array2[h2][w2]=makePattern2(h2-1, w2, lengW2, array2)-1;
        }
        if (((h2%2)==0)&(w2>1)) {
            array2[h2][w2]=makePattern2(h2, w2-1, lengW2, array2)-1;
        }
        return array2[h2][w2];
    }

    private static int makePattern(int h, int w, int lengH, int[][] array) {
        if (((w%2)==1)&(h==(lengH-1))) {
            if (w==1) {
                array[h][w]=1;
                return array[h][w];
            }
            array[h][w]=makePattern(h, w-1, lengH, array)+1;
        }
        if (((w%2)==1)&(h<(lengH-1))) {
            array[h][w]=makePattern(h+1, w, lengH, array)+1;
        }
        if (((w%2)==0)&(h==1)) {
            array[h][w]=makePattern(h, w-1, lengH, array)+1;
        }
        if (((w%2)==0)&(h>1)) {
            array[h][w]=makePattern(h-1, w, lengH, array)+1;
        }
        return array[h][w];
    }
}

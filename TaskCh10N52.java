import java.util.Scanner;
public class TaskCh10N52 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int num;
        System.out.println("Введите число");
        num = scanner.nextInt();
        reverseDigit(num);
    }

    private static int reverseDigit(int num) {
        if (num<10) {
            System.out.print(num);
            return num;
        }
        System.out.print(num%10);
        reverseDigit(num/10);
        return num%10;
    }
}

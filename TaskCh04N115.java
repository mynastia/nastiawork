import java.util.Scanner;
public class TaskCh4N115 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int nYear;
        System.out.println("Введите год");
        nYear = Integer.valueOf(reader.nextLine());
        int animalNum;
        // Находим животное года по остатку от деления на 12
        String animal = "Not set";
        switch (nYear%12) {
            case 0:
                animal = "Крыса";
                break;
            case 1:
                animal = "Крыса";
                break;
            case 2:
                animal = "Корова";
                break;
            case 3:
                animal = "Тигр";
                break;
            case 4:
                animal = "Заяц";
                break;
            case 5:
                animal = "Дракон";
                break;
            case 6:
                animal = "Змея";
                break;
            case 7:
                animal = "Лошадь";
                break;
            case 8:
                animal = "Овца";
                break;
            case 9:
                animal = "Обезьяна";
                break;
            case 10:
                animal = "Петух";
                break;
            case 11:
                animal = "Собака";
                break;

    }
        // Находим цвет года, вычислив количество 12-летних циклов и остаток от деления данного количества на 5
        String color = "Not set";
        switch ((nYear/12)%5) {
            case 0:
                color = "Черный";
                break;
            case 1:
                color = "Зеленый";
                break;
            case 2:
                color = "Красный";
                break;
            case 3:
                color = "Желтый";
                break;
            case 4:
                color = "Белый";
                break;
        }
            System.out.println(animal+", "+color);
        }
    }


import java.util.Scanner;
public class TaskCh12N63 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int numParallels, numClasses;
        numParallels=12;
        numClasses=5;
        int[][] classes = new int [numParallels][numClasses];
        int[] sum = new int[numParallels];
        int[] avr = new int[numParallels];
        for (int i=1; i<numParallels; i++) {
            sum[i]=0;
        }
        for (int i=1; i<numParallels; i++) {
            for (int j=1; j<numClasses; j++) {
                System.out.println("Введите количество учеников в "+i+"-ом классе номер "+j);
                classes[i][j]=scanner.nextInt();
            }
        }
        for (int i=1; i<numParallels; i++) {
            for (int j=1; j<numClasses; j++) {
                sum[i]=sum[i]+classes[i][j];
            }
        }
        for (int i=1; i<numParallels; i++) {
            avr[i]=sum[i]/4;
            System.out.println("Среднее число учеников в классах параллели "+i+" равно "+avr[i]);
        }
    }
}

import java.util.Scanner;
public class TaskCh4N36 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int t;
        System.out.println("Введите время (в минутах)");
        t = Integer.valueOf(reader.nextLine());
        if ((t%5)<=3) System.out.println("Горит зеленый сигнал");
        if ((t%5)>3) System.out.println("Горит красный сигнал");
    }
}

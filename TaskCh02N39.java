import java.util.Scanner;
public class TaskCh2N39 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int h, m, s;
        System.out.println("Введите часы");
        h = Integer.valueOf(reader.nextLine());
        System.out.println("Введите минуты");
        m = Integer.valueOf(reader.nextLine());
        System.out.println("Введите секунды");
        s = Integer.valueOf(reader.nextLine());
        int hAngle;
        // Находим угол между двумя соседними часами
        hAngle = 360/12;
        // Находим разницу в часах между началом суток и текущим моментом времени
        int hDif = 12 - h;
        // Находим разницу в углах по разнице в часах
        int angleDif = hDif*hAngle;
        System.out.println("Угол между положением часовой стрелки в начале суток и сейчас равен "+angleDif);
    }
}

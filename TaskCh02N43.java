import java.util.Scanner;
public class TaskCh2N43 {
public static void main(String args[]) {
   int a, b;
   Scanner reader = new Scanner(System.in);
   System.out.println("Введите первое число");
   a = Integer.valueOf(reader.nextLine());
   System.out.println("Введите второе число");
    b = Integer.valueOf(reader.nextLine());
   int c1, c2, c3;
   // Находим остатки от деления a и b друг на друга
   c1 = a % b;
   c2 = b % a;
   // Если хотя бы один из остатков равен 0, сумма c3 будет равна 1; в противном случае нет
   c3 = c1*c2+1;
   System.out.println(c3);
    }
}

public class TaskCh5N64 {
    public static void main(String args[]) {
        double[] numPeople = new double[12];
        double[] area = new double[12];
        double[] density =new double[12];
        double totalDensity, avrDensity;
        for (int n=1; n<=11; n++) {
            numPeople[n] = Math.random();
            area[n] = Math.random();
            density[n] = numPeople[n]/area[n];
        }
        totalDensity = 0;
        for (int n=1; n<=11; n++) {
            totalDensity = totalDensity + density[n];
        }
        avrDensity = totalDensity/12;
        System.out.println("Средняя плотность наседения по области равна "+avrDensity);

    }
}

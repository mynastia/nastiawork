import java.util.Scanner;
public class TaskCh10N43 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите число");
        int num;
        num = Integer.valueOf(reader.nextLine());
        int sum;
        int length = String.valueOf(num).length();
        sum=sumOfDigit(num, length);
        System.out.println("Сумма знаков равна "+sum);

    }

    private static int sumOfDigit(int n, int length) {
        int sum;
        if (length==1) {
            sum=n%10;
            return sum;
        }
        sum= (int) ((n%Math.pow(10,length))/Math.pow(10,length-1) + sumOfDigit(n,length-1));
        return sum;
    }}
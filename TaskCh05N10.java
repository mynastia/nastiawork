import java.util.Scanner;
public class TaskCh5N10 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int course, dollars;
        System.out.println("Введите текущий курс рубля к доллару");
        course = Integer.valueOf(reader.nextLine());
        for (dollars=1; dollars<=20; dollars++) {
            System.out.println(dollars+" долларов, "+dollars*course+" рублей");
        }
    }
}

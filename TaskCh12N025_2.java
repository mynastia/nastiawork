import java.util.Scanner;
public class TaskCh12N25_2 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int i, j;
        int leng;
        System.out.println("Введите размер массива");
        leng=scanner.nextInt();
        int [][] array = new int [leng][leng];
        for (i=1; i<leng; i++) {
            for (j=1; j<leng; j++) {
                array[i][j]=makePattern1(i, j, array);
                System.out.print(array[i][j]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int h, w; //h - высота таблицы, w - длина таблицы
        int lengH, lengW;
        System.out.println("Введите ширину массива");
        lengW=scanner.nextInt();
        System.out.println("Введите длину массива");
        lengH=scanner.nextInt();
        int[][] array2 = new int[lengH][lengW];
        for (h=1; h<lengH; h++) {
            for (w=1; w<lengW; w++) {
                array2[h][w]=makePattern2(h, w, lengW, array2);
                System.out.print(array2[h][w]+", ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
        int h3, w3;
        int lengH3, lengW3;
        lengH3=13;
        lengW3=11;
        int [][] array3 = new int [lengH3][lengW3];
        for (h3=1; h3<lengH3; h3++) {
            for (w3=1; w3<lengW3; w3++) {
            array3[h3][w3]=makePattern3(h3, w3, array3);
                System.out.print(array3[h3][w3]+", ");

    }
            System.out.println(" ");


    }
        System.out.println(" ");
        int h4, w4;
        int lengH4, lengW4;
        lengH4=13;
        lengW4=11;
        int [][] array4 = new int [lengH4][lengW4];
        for (h4=1; h4<lengH4; h4++) {
            for (w4=1; w4<lengW4; w4++) {
                array4[h4][w4]=makePattern4(h4, w4, array4);
                System.out.print(array4[h4][w4]+", ");
            }
            System.out.println(" ");

}

    }

    private static int makePattern4(int h4, int w4, int[][] array4) {
        if (w4==1) {
            array4[h4][w4]=120-(h4-1);
            return array4[h4][w4];
        }
        array4[h4][w4]=makePattern4(h4,w4-1, array4)-12;
        return array4[h4][w4];
    }


    private static int makePattern3(int h3, int w3, int[][] array3) {
        if (w3==1) {
            array3[h3][w3]=120-10*(h3-1);
            return array3[h3][w3];
        }
        array3[h3][w3]=makePattern3(h3, w3-1, array3)-1;
        return array3[h3][w3];
    }


    private static int makePattern2(int h, int w, int lengW, int[][] array2) {
        if (h==1) {
            array2[h][w] = 1 + 11*(lengW-1-w);
            return array2[h][w];
        }
        array2[h][w] = makePattern2(h-1, w, lengW, array2)+1;
        return array2[h][w];
    }

    private static int makePattern1(int i, int j, int[][] array) {
        if ((i==1)&(j==1)) {
            array[i][j]=111;
            return array[i][j]; }
        if (i==1) {
            array[i][j] = 110 + j;
            return array[i][j];
        }
        if ((i>1)&(j==1))
            array[i][j]=makePattern1(i-1, j, array)-10;
        else array[i][j]=makePattern1(i, j-1, array)+1;
        return array[i][j];
    }
    }


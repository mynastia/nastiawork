package TaskOOP;

import java.util.Scanner;

class employer {
    public void hello() {
        System.out.println("hi! introduce yourself and describe your java experience please");
    }
}
abstract  class seeker {

    public interface jobSeeker {
        void hello(String name);
        void describeExperience();
    }
}

  class candidate {
    // Encapsulation principle: class candidate
    String name;





}

 class getJavaJobGrad extends candidate implements seeker.jobSeeker {

    // Используется принцип полиморфизма: interface jobSeeker и его методы используются классами getJavaJobGrad и selfLearner в своих формах
    // Encapsulation principle: data and code wrapped together in class getLavaJobGrad
    public void hello(String name) {
        System.out.println("hi! my name is "+name);
    }

    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews.");
    }

}

 class selfLearner extends candidate implements seeker.jobSeeker {

    // Encapsulation principle: data and code wrapped together in class selfLearner

    public void hello(String name) {
        System.out.println("hi! my name is "+name);
    }

    public void describeExperience() {
        System.out.println("I have been learning Java my myself, nobody examined how thorough is my knowledge and how good is my code.");
    }
}


public class TaskOOP7 {
    public static void main(String args[]) {


        candidate[] candidates = new candidate[10];
        for (int i=0; i<10; i++) {
            candidates[i] = new candidate();
        }
        employer anEmployer = new employer();
        int status, getJavaJobCount, selfLearnerCount;
        getJavaJobCount=1;
        selfLearnerCount=1;
        getJavaJobGrad[] getJavaJobGrads = new getJavaJobGrad[10];
        selfLearner[] selfLearners = new selfLearner[10];
        for (int i=0; i<10; i++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Следующий кандидат является выпускником getJavaJob (введите 1) или самоучкой (введите 2)");
            status=scanner.nextInt();
            // Inheritance OOP principle: classes getJavaJobGrad and selfLearner inherit candidate's variable name and make use of it
            if (status==1) {
               getJavaJobGrads[getJavaJobCount] = new getJavaJobGrad();
               System.out.println("Введите имя кандидата");
                Scanner scanner1 = new Scanner(System.in);
               getJavaJobGrads[getJavaJobCount].name=scanner1.nextLine();
               getJavaJobCount=getJavaJobCount+1;
            }
            if (status==2) {
                selfLearners[selfLearnerCount] = new selfLearner();
                System.out.println("Введите имя кандидата");
                Scanner scanner2 = new Scanner(System.in);
                selfLearners[selfLearnerCount].name=scanner2.nextLine();
                selfLearnerCount=selfLearnerCount+1;
            }
        }

        for (int i=1; i<getJavaJobCount; i++) {
            anEmployer.hello();
            getJavaJobGrads[i].hello(getJavaJobGrads[i].name);
            getJavaJobGrads[i].describeExperience();
            System.out.println(" ");
        }
        for (int i=1; i<selfLearnerCount; i++) {
            anEmployer.hello();
            selfLearners[i].hello(selfLearners[i].name);
            selfLearners[i].describeExperience();
            System.out.println(" ");
        }

        }











}

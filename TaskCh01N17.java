import java.util.Scanner;
public class TaskCh01N17 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        double equation1;
        double x1;
        System.out.println("Введите значение x");
        x1 = Double.valueOf(reader.nextLine());
        equation1 = Math.sqrt(1-Math.pow(Math.sin(x1),2));
        System.out.println("Sqrt(1-(sin x)^2) equals "+equation1);
        double equation2;
        double x2;
        double a, b, c;
        System.out.println("Введите следующее значение x");
        x2 = Double.valueOf(reader.nextLine());
        System.out.println("Введите значение a");
        a = Double.valueOf(reader.nextLine());
        System.out.println("Введите значение b");
        b = Double.valueOf(reader.nextLine());
        System.out.println("Введите значение c");
        c = Double.valueOf(reader.nextLine());
        equation2 = 1/Math.sqrt(a*Math.pow(x2,2)+b*x2+c);
        System.out.println("1/sqrt(a*x^2+b*x+c) equals "+equation2);
        double equation3;
        double x3;
        System.out.println("Введите следующее значение x");
        x3 = Double.valueOf(reader.nextLine());
        equation3 = (Math.sqrt(x3+1)+Math.sqrt(x3-1))/(2*Math.sqrt(x3));
        System.out.println("(Sqrt(x+1)+Sqrt(x-1))/(2*Sqrt(x)) equals "+equation3);
        double equation4;
        double x4;
        System.out.println("Введите следующее значение x");
        x4 = Double.valueOf(reader.nextLine());
        equation4 = Math.abs(x4)+Math.abs(x4+1);
        System.out.println("|x|+|x+1| equals "+equation4);
    }
}

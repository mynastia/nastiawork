import java.util.Scanner;
public class TaskCh10N41_2 {
    public static void main(String args[]) {
       Scanner reader = new Scanner(System.in);
       System.out.println("Введите число для нахождения факториала");
       Double num, num2;
       num = Double.valueOf(reader.nextLine());
       num2 = factorial(num);
       System.out.println("Факториал составляет "+num2);
    }

    private static double factorial(double n) {
        if ((n==1)|(n==0))
            return 1;
        double fact;
        fact = n*factorial(n-1);
        return fact;
    }

}

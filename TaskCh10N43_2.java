import java.util.Scanner;
public class TaskCh10N43_2 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите число");
        int n = Integer.valueOf(reader.nextLine());
        int sum;
        sum=calcDigit(n);
        System.out.println("Количество цифр в числе равно "+sum);
    }

    private static int calcDigit(int n) {
        int result=0;
        if (n<10) return 1;
        if (n>=10) result= calcDigit(n/10)+1;
        return result;
        } }



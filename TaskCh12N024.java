import org.w3c.dom.ls.LSOutput;

import java.sql.SQLOutput;
import java.util.Scanner;
public class TaskCh12N24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int leng;
        int i, j;
        leng = scanner.nextInt();
        int[][] array = new int[leng][leng];
        for (i=1; i<leng; i++) {
            for (j=1; j<leng; j++) {
                array[i][j]=makePattern(leng, i, j, array);
                System.out.print(array[i][j]+", ");

    }
            System.out.println(" ");

    }
        System.out.println("Введите размер следующего массива");
        int leng2;
        leng2=scanner.nextInt();
        int[][] array2 = new int [leng2][leng2];
        for (int k=0; k<leng2; k++) {
            for (int n=0; n<leng2; n++) {
                array2[k][n]= (k+n)%6+1;
                System.out.print(array2[k][n]+", ");
            }
            System.out.println(" ");
        }
}


    private static int makePattern(int leng, int i, int j, int[][]array) {
        if ((i==1)|(j==1)) {
            array[i][j]=1;
            return array[i][j];
        } else
            array[i][j]=makePattern(leng, i-1, j, array)+makePattern(leng,i,j-1,array);
        return array[i][j];

    }

}

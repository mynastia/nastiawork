import java.util.Scanner;
public class TaskCh02N13 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int number;
        System.out.println("Введите число между 100 и 200");
        number = Integer.valueOf(reader.nextLine());
        if ((number < 200) & (number > 100)) {
            number = Integer.reverse(number);
            System.out.println("Результат " + number);
        } else {
            System.out.println("Ошибка. Число должно быть между 100 и 200");

        }
    }
}
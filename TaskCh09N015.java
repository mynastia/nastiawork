import java.util.Scanner;
public class TaskCh9N15 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        String word;
        int k;
        char ch;
        System.out.println("Введите слово");
        word = reader.nextLine();
        System.out.println("Введите номер символа");
        k = Integer.valueOf(reader.nextLine());
        ch = word.charAt(k-1);
        System.out.println("Искомый символ "+ch);
    }
}

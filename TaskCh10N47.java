import java.util.Scanner;
public class TaskCh10N47 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите номер члена последовательности Фибоначчи");
        int k;
        k = Integer.valueOf(reader.nextLine());
        int fib;
        fib=getFib(k);
        System.out.println(k+"-ный член последовательности Фибоначчи равен "+fib);
    }

    private static int getFib(int k) {
        if ((k==1)|(k==2)) return 1;
        int fib = getFib(k-1) + getFib(k-2);
        return fib;
    }
}

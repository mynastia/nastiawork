import java.util.Scanner;
public class TaskCh12N234 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int h, w; // "Height" and "width" of incoming array
        int dH, dW; // "Height" and "width" to delete
        System.out.println("Введите количество строк в массиве");
        h=scanner.nextInt()+1;
        System.out.println("Введите количество столбцов в массиве");
        w=scanner.nextInt()+1;
        int[][] array = new int[h][w];
        for (int i=1; i<h; i++) {
            for (int j=1; j<w; j++) {
                System.out.println("Введите элемент массива строки "+i+", столбца "+j);
                array[i][j]=scanner.nextInt();
            }
        }
        System.out.println("Начальный массив:");
        for (int i=1; i<h; i++) {
            for (int j=1; j<w; j++) {
                System.out.print(array[i][j]+", ");
            }
            System.out.println(" ");
        }
        System.out.println("Введите номер строки для удаления");
        dH=scanner.nextInt();
        for (int i=dH; i<h-1; i++) {
            for (int j=1; j<w; j++) {
                array[i][j]=array[i+1][j];
            }
        }
        for (int j=1; j<w; j++) {
            array[h-1][j]=0;
        }
        System.out.println("Результат удаления строки:");
        for (int i=1; i<h; i++) {
            for (int j=1; j<w; j++) {
                System.out.print(array[i][j]+", ");
            }
            System.out.println(" ");
        }
        System.out.println("Введите номер столбца для удаления");
        dW=scanner.nextInt();
        for (int i=1; i<h; i++) {
            for (int j=dW; j<w-1; j++) {
                array[i][j]=array[i][j+1];
            }
        }
        for (int i=1; i<h; i++) {
            array[i][w-1]=0;
        }
        System.out.println("Результат удаления строки и столбца:");
        for (int i=1; i<h; i++) {
            for (int j=1; j<w; j++) {
                System.out.print(array[i][j]+", ");
            }
            System.out.println(" ");
        }

    }
}

import java.util.Scanner;
public class TaskCh10N48 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите желаемую длину массива");
        int n = reader.nextInt();
        int[] array = new int[n];
        for (int i=0; i<=n-1; i++) {
            System.out.println("Введите элемент массива номер "+i);
            array[i]=reader.nextInt();
        }
        int max;
        max=getMax(array, n);
        System.out.println("Максимальный элемент массива составляет "+max);
    }

    private static int getMax(int [] array, int n) {
        if (n==1) return array[0];
        return Math.max(array[n-1], getMax(array,n-1));
    }
}

import java.util.Scanner;
public class TaskCh10N45 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int a, k, i; //Переменные первого члена прогрессии, разности прогрессии и номера члена прогрессии
        System.out.println("Введите первый член арифметической прогрессии");
        a = Integer.valueOf(reader.nextLine());
        System.out.println("Введите разность арифметической прогрессии");
        k = Integer.valueOf(reader.nextLine());
        System.out.println("Введите номер искомого члена арифметической прогрессии");
        i = Integer.valueOf(reader.nextLine());
        int n; // i-ный член прогрессии
        n=calcProgr(a, k, i);
        System.out.println(i+"-ый член прогрессии равен "+n);
        int sum;
        sum = calcProgrSum(a, k, i);
        System.out.println("Сумма "+i+" членов прогрессии равна "+sum);
    }

    private static int calcProgrSum(int a, int k, int i) {
        if (i==1) return a;
        if (i==2) {
            int sum = 2*a+k;
            return sum;
        }
        int sum = (i-1)*k+a+calcProgrSum(a,k,i-1);
        return sum;

    }

    private static int calcProgr(int a, int k, int i) {
        if (i==1) return a;
        if (i==2) return a+k;
        int n = k + calcProgr(a,k,i-1);
        return n;
    }
}

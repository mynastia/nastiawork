import java.util.Scanner;
public class TaskCh10N55 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int num, newBase;
        System.out.println("Введите число");
        num = scanner.nextInt();
        System.out.println("Введите базу системы счисления для перевода");
        newBase=scanner.nextInt();
        calcBase(num, newBase);

    }

    private static int calcBase(int num, int newBase) {
        if (num<newBase) {
            System.out.print(num);
            return num;
        }
        System.out.print(num%newBase);
        num=num/newBase;
        calcBase(num,newBase);
        return num%newBase;
    }
}

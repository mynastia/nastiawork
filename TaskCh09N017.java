import java.util.Scanner;
public class TaskCh9N17 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = reader.nextLine();
        char firstCh = word.charAt(1);
        char lastCh = word.charAt(word.length()-1);
        if (firstCh==lastCh) {
            System.out.println("Слово начинается и заканчивается на одну букву");
        } else {
            System.out.println("Слово начинается и заканчивается на разные буквы");
    }
}}

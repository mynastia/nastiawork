import java.util.Scanner;
public class TaskCh4N15 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int yBirth, mBirth, yToday, mToday;
        int yAge;
        System.out.println("Введите год рождения");
        yBirth = Integer.valueOf(reader.nextLine());
        System.out.println("Введите месяц рождения");
        mBirth = Integer.valueOf(reader.nextLine());
        System.out.println("Введите текущий год");
        yToday = Integer.valueOf(reader.nextLine());
        System.out.println("Введите текущий месяц");
        mToday = Integer.valueOf(reader.nextLine());
        if ((12-mBirth+mToday)<12) {
            yAge = yToday - yBirth - 1;
        } else {
            yAge = yToday - yBirth;    }
        System.out.println("Возраст составляет "+yAge+" полных лет");
}}

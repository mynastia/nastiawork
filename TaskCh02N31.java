import java.util.Scanner;
public class TaskCh2N31 {
    public static void main(String args[]) {
       Scanner reader = new Scanner(System.in);
       int n, n2, x;
       int a, b, c;
        System.out.println("Введите трехзначное число n");
        n = Integer.valueOf(reader.nextLine());
        b = n % 10;
        n2 = (n-b)/10;
        c = n2 % 10;
        a = (n2-c)/10;
        x = a*100 + b*10 + c;
        System.out.println("Исходное число x равно "+x);
    }
}

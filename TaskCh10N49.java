import java.util.Scanner;
public class TaskCh10N49 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите желаемую длину массива");
        int n = reader.nextInt();
        int[] array = new int[n];
        for (int i=0; i<=n-1; i++) {
            System.out.println("Введите элемент массива номер "+i);
            array[i]=reader.nextInt();
        }
        int max;
        max=getMax(array, n);
        int index;
        index=getIndex(array, max);
        System.out.println("Индекс максимального элемента составляет "+index);

    }

    private static int getIndex(int [] array, int element) {
        int i = 0;
        int len = array.length;
        while (i<len)
            if (array[i]==element) {
                return i;
            } else {
                i = i+1;
            }
        return -1;

    }

    private static int getMax(int [] array, int n) {
        if (n==1) return array[0];
        return Math.max(array[n-1], getMax(array,n-1));
    }
}

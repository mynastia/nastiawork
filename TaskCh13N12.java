package TaskCh13N12_2;

public class TaskCh13N12 {
    public static void main(String args[]) {
        Employee employee[] = new Employee[20];

        employee[0] = new Employee("Бирюков", "Федор", "Виталиевич", "ул. Самаринская 2-я, дом 32, квартира 65", 6, 2017);
        employee[1] = new Employee("Шумейко",  "Шерлок", "Федорович", "ул. Проселочный пер, дом 99, квартира 201", 7, 2019);
        employee[2] = new Employee("Воробьев", "Глеб", "Федорович", "ул. СибВО Ельцовка тер, дом 35, квартира 483", 12, 2018);
        employee[3] = new Employee("Колесников", "Остин", "ул. Восточный проезд, дом 69, квартира 323", 9, 2019);
        employee[4] = new Employee("Осипов", "Семен", "Борисович", "ул. Совхозная, дом 62, квартира 25", 5, 2018);
        employee[5] = new Employee("Шестаков", "Гарри", "Ехимович", "ул. Панаева, дом 28, квартира 151", 4, 2019);
        employee[6] = new Employee("Алчевский", "Альберт", "Данилович", "ул. Пролетарская, дом 4, квартира 15", 8, 2018 );
        employee[7] = new Employee("Шевченко", "Орландо", "Петрович", "ул. Кржижановского, дом 23, квартира 254", 4, 2016);
        employee[8] = new Employee("Макаров", "Феликс", "Эдуардович", "ул. Сыромятнический 4-й пер, дом 79, квартира 199", 2, 2018);
        employee[9] = new Employee("Милославский", "Карл", "ул. Маршала Бабаджаняна пл, дом 70, квартира 471", 2, 2020);
        employee[10] = new Employee("Бородай", "Альберт", "Петрович", "ул. Декабрьская Б., дом 60, квартира 258", 9, 2018);
        employee[11] = new Employee("Назаров", "Эрик", "Романович", "ул. Жебинский б-р, дом 1, квартира 171", 7, 2017);
        employee[12] = new Employee("Красильников", "Йомер", "Дмитриевич", "ул. Орудийная, дом 73, квартира 227", 9, 2016);
        employee[13] = new Employee("Ермаков", "Алексей", "Петрович", "ул. Орудийная, дом 73, квартира 227", 8, 2016);
        employee[14] = new Employee("Зиновьев", "Эрик", "Андреевич","ул. Академика Янгеля, дом 19, квартира 427", 5, 2015 );
        employee[15] = new Employee("Мартынов", "Йозеф", "Евгеньевич", "ул. Есенина, дом 23, квартира 146", 11, 2019);
        employee[16] = new Employee("Симонов", "Иннокентий", "Владимирович", "ул. Вишневая, дом 78, квартира 286", 9, 2016);
        employee[17] = new Employee("Захаров", "Матвей", "Виталиевич", "ул. Черной речки наб, дом 66, квартира 284", 3, 2019);
        employee[18] = new Employee("Крылов", "Любомир", "Алексеевич",  "ул. Рахмановский пер, дом 25, квартира 355", 6, 2016);
        employee[19] = new Employee("Чухрай", "Петр", "Данилович", "ул. Пушкарсркая Малая, дом 16, квартира 414", 9, 2018);
        new Database(employee);

        System.out.println("На сегодняшний день проработали не менее 3-х лет сотрудники:");
        for (int i = 0; i < 20; i++) {
            if (Employee.getWorkExperienceOnDate(employee[i].startMonth, employee[i].startYear) >= 3) {
                System.out.print(employee[i].lastName + " ");
                System.out.print(employee[i].name + " ");
                System.out.print(employee[i].middleName + " ");
                System.out.print(employee[i].address + " ");
                System.out.println(" ");
            }
        }
    }



    }

      class Employee {
        String lastName = " ";
        String name = " ";
        String middleName = " ";
        String address = " ";
        int startMonth = 1;
        int startYear = 1;

          public static int getWorkExperienceOnDate(int month, int year) {
              int nowMonth, nowYear;
              nowMonth = 4;
              nowYear = 2020;
              int workExperience = nowYear - year;
              if ((nowMonth + 12 - month) >= 12) {
                  workExperience = workExperience + 1;
              }
              return workExperience;
          }

        Employee(String lastName, String name, String middleName, String address, int startMonth, int startYear) {
            if (middleName==null) new Employee(lastName, name, address, startMonth, startYear);
            this.lastName=lastName;
            this.name=name;
            this.middleName=middleName;
            this.address=address;
            this.startMonth=startMonth;
            this.startYear=startYear;
        }
          Employee(String lastName, String name, String address, int startMonth, int startYear) {
              this.lastName=lastName;
              this.name=name;
              this.middleName="";
              this.address=address;
              this.startMonth=startMonth;
              this.startYear=startYear;
          }
          Employee() {

          }

    }
    class Database {
        static String lastName;
        static String name;
        static String middleName;
        static String address;
        int startMonth;
        int startYear;
        static Database [] database = new Database[20];
        static Employee[] baseEmployee = new Employee[20];


    Database(Employee[] employee) {
        for (int i=0; i<20; i++) {
            baseEmployee[i] = new Employee();
        }
        baseEmployee=employee;
    }
    public static Employee findEmployees(String fullNameSubstring) {
        boolean isIt;
        for (int i=0; i<20; i++) {
            isIt =baseEmployee[i].lastName.toLowerCase().contains(fullNameSubstring);
            if (isIt==true) {
                return baseEmployee[i];
            }
            isIt = baseEmployee[i].name.toLowerCase().contains(fullNameSubstring);
            if (isIt==true) {
                return baseEmployee[i];
            }
            isIt = baseEmployee[i].middleName.toLowerCase().contains(fullNameSubstring);
            if (isIt==true) {
                return baseEmployee[i];
            }
        }
        return null;
    }
    public static Employee findEmployees(int workingYears) {
        int nowMonth = 4;
        int nowYear = 2020;

        for (int i=0; i<20; i++) {
            int employeeWorked;
            employeeWorked= nowYear - baseEmployee[i].startYear;
            if ((nowMonth + 12 - baseEmployee[i].startMonth)>=12) {
                employeeWorked=employeeWorked+1;
            }
            if (employeeWorked>=workingYears) {
                return baseEmployee[i];
            }

    }
        return null;
    }}


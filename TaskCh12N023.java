import java.sql.SQLOutput;
import java.util.Scanner;

public class TaskCh12N23 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер двумерного массива (нечетное число)");
        int num;
        num = scanner.nextInt();
        int[][] array = new int[num][num];
        for (int i = 1; i<num; i++) {
            for (int j=1; j<num; j++) {

                if (i==j) {
                    array[i][j]=1;
                    System.out.print(array[i][j]+", ");
                } else if (j==(num-i)) {
                    array[i][j]=1;
                    System.out.print(array[i][j]+", ");
                } else if (i!=j) {
                    array[i][j]=0;
                    System.out.print(array[i][j]+", ");
                }
        }
            System.out.println(" ");
    }
        System.out.println("Введите размер следующего двумерного массива (нечетное число)");
        int num2;
        num2 = scanner.nextInt();
        int [][] array2 = new int[num2][num2];
        for (int i=1; i<num2; i++) {
            for (int j=1; j<num2; j++) {
                if (i==j) {
                    array2[i][j] = 1;
                    System.out.print(array2[i][j]+", ");
                } else if (i==(num2-j)) {
                    array2[i][j]=1;
                    System.out.print(array2[i][j]+", ");
                } else if (i==(num2/2)) {
                    array2[i][j] = 1;
                    System.out.print(array2[i][j]+", ");
                } else if (j==(num2/2)) {
                    array2[i][j] = 1;
                    System.out.print(array2[i][j]+", ");
                } else {
                    array2[i][j] = 0;
                    System.out.print(array2[i][j]+", ");

                }
            }
            System.out.println(" ");
        }
        System.out.println("Введите размер следующего двумерного массива (нечетное число)");
        int num3;
        num3 = scanner.nextInt();
        int [][] array3 = new int[num3][num3];
        for (int i=1; i<num3; i++) {
            for (int j=1; j<num3; j++) {
                if (j<=(num3/2)) {
                    if (i<=(num3/2)) {
                        if (i<=j) {
                            array3[i][j]=1;
                            System.out.print(array3[i][j]+", ");
                        } else if (i>j) {
                            array3[i][j]=0;
                            System.out.print(array3[i][j]+", ");
                        }
                    } else if (i>(num3/2)) {
                        if (i<=(num3-j)) {
                            array3[i][j]=0;
                            System.out.print(array3[i][j]+", ");
                        } else if (i>(num3-j)) {
                            array3[i][j]=1;
                            System.out.print(array3[i][j]+", ");
                        }

                    }
                }
                else if (j>(num3/2)) {
                    if (i<=(num3/2)) {
                        if (i<=(num3-j)) {
                            array3[i][j]=1;
                            System.out.print(array3[i][j]+", ");
                        } else if (i>(num3-j)) {
                            array3[i][j]=0;
                            System.out.print(array3[i][j]+", ");
                        }
                    } else if (i>(num3/2)) {
                        if (i<=j) {
                            array3[i][j]=0;
                            System.out.print(array3[i][j]+", ");
                        } else if (i>j) {
                            array3[i][j]=1;
                            System.out.print(array3[i][j]+", ");
                        }
                    }

                }
            }
            System.out.println(" ");
        }

}}

import java.util.Scanner;
public class TaskCh10N46 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int a, n, i; //Переменные первого члена, разности и номера искомого члена геометрической прогрессии
        System.out.println("Введите первый член геометрической прогрессии");
        a = Integer.valueOf(reader.nextLine());
        System.out.println("Введите разность геометрической прогрессии");
        n = Integer.valueOf(reader.nextLine());
        System.out.println("Введите номер искомого члена геометрической прогрессии");
        i = Integer.valueOf(reader.nextLine());
        int num=getProgr(a, n, i);
        System.out.println(i+"-ный член прогрессии составляет "+num);
        int sum;
        sum=getSum(a, n, i);
        System.out.println("Сумма членов геометрической прогресии составляет "+sum);
    }

    private static int getSum(int a2, int n2, int i2) {
        if (i2==1) return a2;
        int sum = (int) Math.pow(n2,i2-1)*a2+getSum(a2,n2,i2-1);
        return sum;
    }

    private static int getProgr(int a, int n, int i) {
        if (i==1) return a;
        if (i==2) return n*a;
        int num=n*getProgr(a,n,i-1);
        return num;
    }
}

import java.util.Scanner;
public class TaskCh10N53 {
    public static void main(String args[]) {
       Scanner scanner = new Scanner(System.in);
        int i;
        System.out.println("Введите желаемую длину массива");
        i = scanner.nextInt();
        int [] array = new int[i];
        for (int k=1; k<=i-1; k++) {
            System.out.println("Введите элемент массива номер "+k);
            array[k] = scanner.nextInt();
        }
        reversePrint(i-1, array);
    }

    private static int reversePrint(int i, int[] array) {
        if (i==0) {
            System.out.print(array[0]);
            return array[0];
        }
        System.out.print(array[i]);
        reversePrint(i-1, array);
        return array[i];
    }
}

import java.util.Scanner;
public class TaskCh9N22 {
    public static void main (String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите слово из четного числа букв");
        String word = reader.nextLine();
        String wordHalf = word.substring(0, (word.length()/2));
        System.out.println("Результат: "+wordHalf);
    }
}

import java.util.Scanner;
public class TaskCh3N29 {
    public static void main(String args[]) {
        /** Условие: каждое из чисел x и y нечетное */
       Scanner reader = new Scanner(System.in);
        int x, y, z;
        System.out.println("Введите x");
        x = Integer.valueOf(reader.nextLine());
        System.out.println("Введите y");
        y = Integer.valueOf(reader.nextLine());
        System.out.println("Введите z");
        z = Integer.valueOf(reader.nextLine());
        boolean cond1 = (x%2)+(y%2)==2;
        if (cond1) System.out.println("Условие 1 выполняется");
        /** Условие: только одно из чисел x и y меньше 20 */
        boolean cond2 = (x<=20)^(y<=20);
        if (cond2) System.out.println("Условие 2 выполняется");
        /** Условие: хотя бы одно из чисел x и y равно 0 */
        boolean cond3 = x*y==0;
        if (cond3) System.out.println("Условие 3 выполняется");
        /** Условие: каждое из чисел x, y, z отрицательное */
        boolean cond4 = (x<0)&(y<0)&(z<0);
        if (cond4) System.out.println("Условие 4 выполняется");
        /** Условие: только одно из чисел x, y, z кратно 5 */
        /** Пояснение к решению: В случае, если только одно число из x, y, z делится на 5,
        в составе числа x*y*z будет только одна 5, и соответсвенно разделенное на 5 оно должно делиться на 5 с остатком */
         boolean cond5 = ((x*y*z)/5)%5!=0;
         if (cond5) System.out.println("Условие 5 выполняется");
        /** Условие: хотя бы одно из чисел x, y, z больше 100 */
        boolean cond6 = ((x-100)>0)|((y-100)>0)|((z-100)>0);
        if (cond6) System.out.println("Условие 6 выполняется");
    }
}

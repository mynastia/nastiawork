import java.util.Scanner;
public class TaskCh10N42 {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите число");
        double a;
        a = Double.valueOf(reader.nextLine());
        System.out.println("Введите степень, в которую возвести число");
        int n;
        n = Integer.valueOf(reader.nextLine());
        double a2;
        a2=toPower(a, n);
        System.out.println("Результат "+a2);

    }

    private static double toPower(double a, int n) {
        if (n==1)
            return a;
        if (n==0)
            return 1;
        double result;
        result = a*toPower(a,n-1);
        return result;
    }
}
